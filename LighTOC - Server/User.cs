﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace LighTOCServer
{
    class User
    {
        public TcpClient client { get; private set; }
        public BinaryReader br { get; private set; }
        public BinaryWriter bw { get; private set; }
        public string userName { get; set; }
        public string userAddress { get; set; }
        public int userPort { get; set; }
        public User(TcpClient client)
        {
            this.client = client;
            NetworkStream networkStream = client.GetStream();
            br = new BinaryReader(networkStream);
            bw = new BinaryWriter(networkStream);
            userAddress = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();
        }

        public void Close()
        {
            br.Close();
            bw.Close();
            client.Close();
        }
    }
}
