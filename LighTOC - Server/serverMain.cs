﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace LighTOCServer
{
    public partial class serverMain : MetroFramework.Forms.MetroForm
    {

        //DES vector
        private static byte[] DES_IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
        //AES vector
        public static readonly byte[] AES_IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
        private List<User> userList = new List<User>();
        private const int port = 13789;
        private TcpListener myListener;
        bool isDisconnect = false;

        public serverMain()
        {
            InitializeComponent();
            //IPAddress localAddress = IPAddress.Parse(GetServerLocalIPv4Address());
            lboxLog.HorizontalScrollbar = true;
            btnStop.Enabled = false;
            lblLocalAddressIPv4.Text = GetServerLocalIPv4Address();
        }

        private string GetServerLocalIPv4Address()
        {
            string strLocalIP = string.Empty;
            try
            {
                IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
                for (int i = 0; i<ipHost.AddressList.Length; i++)
                {
                    if (ipHost.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                        return ipHost.AddressList[i].ToString();
                }
                return "Cannot get IP";
            }
            catch (Exception ex)
            {
                MetroFramework.MetroMessageBox.Show(this, ex.Message, "Cannot get local IP", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Cannot get IP";
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            lblStatus.ForeColor = Color.Green;
            lblStatus.Text = "ON";
            myListener = new TcpListener(IPAddress.Parse(lblLocalAddressIPv4.Text), port);
            myListener.Start();
            AddItemToListBox(string.Format("Server begins to monitor connecting request {0}:{1}", lblLocalAddressIPv4.Text, port));
            Thread myThread = new Thread(ListenClientConnect);
            myThread.Start();
            btnStart.Enabled = false;
            btnStart.BackColor = Color.LightGray; 
            btnStop.Enabled = true;
            btnStop.BackColor = Color.DeepSkyBlue;
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            AddItemToListBox("Stop Service! Users log out");
            isDisconnect = true;
            for (int i = userList.Count - 1; i >= 0; i--)
            {
                RemoveUser(userList[i]);
            }
            //通过停止监听让myListener.AcceptTcpClient()产生异常退出监听线程
            myListener.Stop();
            btnStart.Enabled = true;
            btnStart.BackColor = Color.DeepSkyBlue;
            btnStop.Enabled = false;
            btnStop.BackColor = Color.LightGray;
            lblStatus.ForeColor = Color.Red;
            lblStatus.Text = "OFF";
            SaveLstToTxt(lboxLog);
        }
        private void SaveLstToTxt(ListBox lst)
        {
            string sPath = @"C:\Users\hawkmoth\Desktop\work logs\worklogs.txt";
            FileStream fs = new FileStream(sPath, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
            int iCount = lst.Items.Count - 1;
            for (int i = 0; i <= iCount; i++)
            {
                sw.WriteLine(lst.Items[i].ToString());
            }
            sw.Flush();
            sw.Close();
            fs.Close();

        }
        private void FormServer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (myListener != null)
            {
                btnStop.PerformClick();
            }
        }

        delegate void AddItemToListBoxDelegate(string str);
        // 在lboxLog中记录工作日志
        // <param name="str">要追加的信息</param>
        private void AddItemToListBox(string str)
        {
            if (lboxLog.InvokeRequired)
            {
                AddItemToListBoxDelegate d = AddItemToListBox;
                lboxLog.Invoke(d, str);
            }
            else
            {
                lboxLog.Items.Add(str);
                lboxLog.SelectedIndex = lboxLog.Items.Count - 1;
                lboxLog.ClearSelected();
            }
        }

        private void ListenClientConnect()
        {
            TcpClient newClient = null;
            while (true)
            {
                ListenClientDelegate d = new ListenClientDelegate(ListenClient);
                IAsyncResult result = d.BeginInvoke(out newClient, null, null);
                //使用轮询方式来判断异步操作是否完成
                while (result.IsCompleted == false)
                {
                    if (isDisconnect)
                        break;
                    Thread.Sleep(250);
                }
                //获取Begin 方法的返回值和所有输入/输出参数
                d.EndInvoke(out newClient, result);
                if (newClient != null)
                {
                    //每接受一个客户端连接，就创建一个对应的线程循环接收该客户端发来的信息
                    User user = new User(newClient);
                    Thread threadReceive = new Thread(ReceiveData);
                    threadReceive.Start(user);
                    userList.Add(user);
                    AddItemToListBox(string.Format("[{0}] Login", newClient.Client.RemoteEndPoint));
                    AddItemToListBox(string.Format("Users Online: {0}", userList.Count));
                }
                else
                {
                    break;
                }
            }
        }

        private delegate void ListenClientDelegate(out TcpClient client);
        // 接受客户端的连接请求
        // <param name="newClient"></param>
        private void ListenClient(out TcpClient newClient)
        {
            try
            {
                newClient = myListener.AcceptTcpClient();
            }
            catch
            {
                newClient = null;
            }
        }
        private string digsig = "";
        private void ReceiveData(object userState)
        {
            
            User user = (User)userState;
            TcpClient client = user.client;
            while (!isDisconnect)
            {
                string receiveString = null;
                ReceiveMessageDelegate d = new ReceiveMessageDelegate(ReceiveMessage);
                IAsyncResult result = d.BeginInvoke(user, out receiveString, null, null);
                //使用轮询方式来判断异步操作是否完成
                while (!result.IsCompleted)
                {
                    if (isDisconnect)
                        break;
                    Thread.Sleep(250);
                }
                //获取Begin方法的返回值和所有输入/输出参数
                d.EndInvoke(out receiveString, result);
                if (receiveString == null)
                {
                    if (!isDisconnect)
                    {
                        AddItemToListBox(string.Format("{0} disconnect，stop receiving", client.Client.RemoteEndPoint));
                        RemoveUser(user);
                    }
                    break;
                }
                AddItemToListBox(string.Format("From [{0}]", user.client.Client.RemoteEndPoint));
                string[] splitString = receiveString.Split(',');
                switch (splitString[0])
                {
                    case "Login":
                        user.userName = splitString[1];
                        AsyncSendToAllClient(user, receiveString);
                        break;
                    case "Logout":
                        AsyncSendToAllClient(user, receiveString);
                        RemoveUser(user);
                        break;
                    case "Talk":
                        string talkString = receiveString.Substring(splitString[0].Length + splitString[1].Length + splitString[2].Length + splitString[3].Length + splitString[4].Length + splitString[5].Length + 6);                    
                        AddItemToListBox(string.Format("{0} talks to {1}", user.userName, splitString[1]));
                        digsig = splitString[2];
                        string color = splitString[3];
                        string font = splitString[4];
                        string enname = splitString[5];
                        foreach (User target in userList)
                        {
                            if (target.userName == splitString[1])
                            {
                                AsyncSendToClient(target, "talk," + user.userName + ","+ digsig + ","+color+","+font+"," +enname+","+ talkString);
                                break;
                            }
                        }
                        break;
                    case "File":
                        string filename = splitString[3];
                        string str = receiveString.Substring(splitString[0].Length + splitString[1].Length + splitString[2].Length + splitString[3].Length +  4);
                        AddItemToListBox(string.Format("{0} sends file transfer request to {1}", user.userName, splitString[1]));
                        foreach(User target in userList)
                        {
                            if (target.userName == splitString[1])
                            {
                                AsyncSendToClient(target, "file," + user.userName + ',' + filename +','+str);
                            }
                        }
                        break;
                    case "Accept":
                        AddItemToListBox(string.Format("{0} receive file transimitting request from {1}", user.userName, splitString[1]));
                        
                        foreach (User target in userList)
                        {
                            if (target.userName == splitString[1])
                            {
                                AsyncSendToClient(target, "accept," + user.userName );
                            }
                        }
                        break;
                    default:
                        AddItemToListBox("Receieve information beyond protocols" + receiveString);
                        break;
                }
            }
        }
        delegate void ReceiveMessageDelegate(User user, out string receiveMessage);
        // 接收客户端发来的信息
        // <param name="user"></param>
        // <param name="receiveMessage"></param>
        private void ReceiveMessage(User user, out string receiveMessage)
        {
            try
            {
                receiveMessage = user.br.ReadString();
            }
            catch (Exception ex)
            {
                AddItemToListBox(ex.Message);
                receiveMessage = null;
            }
        }
        private void RemoveUser(User user)
        {
            userList.Remove(user);
            user.Close();
            AddItemToListBox(string.Format("Users Online: {0}", userList.Count));
        }
        private void AsyncSendToAllClient(User user, string message)
        {
            string command = message.Split(',')[0].ToLower();
            if (command == "login")
            {
                for (int i = 0; i < userList.Count; i++)
                {
                    AsyncSendToClient(userList[i], message + "," + userList[i].userAddress);
                    if (userList[i].userName != user.userName)
                        AsyncSendToClient(user, "login," + userList[i].userName + "," + userList[i].userAddress);
                }
            }
            else if (command == "logout")
            {
                for (int i = 0; i < userList.Count; i++)
                {
                    if (userList[i].userName != user.userName)
                        AsyncSendToClient(userList[i], message);
                }
            }
        }
        private void AsyncSendToClient(User user, string message)
        {
            SendToClientDelegate d = new SendToClientDelegate(SendToClient);
            IAsyncResult result = d.BeginInvoke(user, message, null, null);
            while (result.IsCompleted == false)
            {
                if (isDisconnect)
                    break;
                Thread.Sleep(250);
            }
            d.EndInvoke(result);
        }
        private delegate void SendToClientDelegate(User user, string message);
        // 发送message给user
        // <param name="user"></param>
        // <param name="message"></param>
        private void SendToClient(User user, string message)
        {
            try
            {
                //将字符串写入网络流，此方法会自动附加字符串长度前缀
                user.bw.Write(message);
                user.bw.Flush();
                AddItemToListBox(string.Format("Send to[{0}]", user.userName));
            }
            catch
            {
                AddItemToListBox(string.Format("Message send failed to[{0}]", user.userName));
            }
        }

        private void serverMain_Load(object sender, EventArgs e)
        {

        }

        #region AES
        /// <summary>  
        /// AES加密算法  
        /// </summary>  
        /// <param name="input">明文字符串</param>  
        /// <param name="key">密钥</param>  
        /// <returns>字符串</returns>  
        public static string EncryptByAES(string input, string key)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key.Substring(0, 32));
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = keyBytes;
                aesAlg.IV = AES_IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(input);
                        }
                        byte[] bytes = msEncrypt.ToArray();
                        //return Convert.ToBase64String(bytes);//此方法不可用
                        return BitConverter.ToString(bytes);
                    }
                }
            }
        }
        /// <summary>  
        /// AES解密  
        /// </summary>  
        /// <param name="input">密文字节数组</param>  
        /// <param name="key">密钥</param>  
        /// <returns>返回解密后的字符串</returns>  
        public static string DecryptByAES(string input, string key)
        {
            //byte[] inputBytes = Convert.FromBase64String(input); //Encoding.UTF8.GetBytes(input);
            string[] sInput = input.Split("-".ToCharArray());
            byte[] inputBytes = new byte[sInput.Length];
            for (int i = 0; i < sInput.Length; i++)
            {
                inputBytes[i] = byte.Parse(sInput[i], NumberStyles.HexNumber);
            }
            byte[] keyBytes = Encoding.UTF8.GetBytes(key.Substring(0, 32));
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = keyBytes;
                aesAlg.IV = AES_IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream msEncrypt = new MemoryStream(inputBytes))
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srEncrypt = new StreamReader(csEncrypt))
                        {
                            return srEncrypt.ReadToEnd();
                        }
                    }
                }
            }
        }
        /// <summary> 
        /// AES加密        
        /// </summary> 
        /// <param name="inputdata">输入的数据</param>         
        /// <param name="iv">向量128位</param>         
        /// <param name="strKey">加密密钥</param>         
        /// <returns></returns> 
        public static byte[] EncryptByAES(byte[] inputdata, byte[] key, byte[] iv)
        {
            ////分组加密算法 
            //Aes aes = new AesCryptoServiceProvider();          
            ////设置密钥及密钥向量 
            //aes.Key = key;
            //aes.IV = iv;
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
            //    {
            //        using (StreamWriter writer = new StreamWriter(cs))
            //        {
            //            writer.Write(inputdata);
            //        }
            //        return ms.ToArray(); 
            //    }               
            //}

            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = key;
                aesAlg.IV = iv;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(inputdata);
                        }
                        byte[] encrypted = msEncrypt.ToArray();
                        return encrypted;
                    }
                }
            }
        }
        /// <summary>         
        /// AES解密         
        /// </summary> 
        /// <param name="inputdata">输入的数据</param>                
        /// <param name="key">key</param>         
        /// <param name="iv">向量128</param> 
        /// <returns></returns> 
        public static byte[] DecryptByAES(byte[] inputBytes, byte[] key, byte[] iv)
        {
            Aes aes = new AesCryptoServiceProvider();
            aes.Key = key;
            aes.IV = iv;
            byte[] decryptBytes;
            using (MemoryStream ms = new MemoryStream(inputBytes))
            {
                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    using (StreamReader reader = new StreamReader(cs))
                    {
                        string result = reader.ReadToEnd();
                        decryptBytes = Encoding.UTF8.GetBytes(result);
                    }
                }
            }

            return decryptBytes;
        }
        #endregion

        #region DSA
        #endregion

        #region RSA
        /// <summary>
        /// RSA加密
        /// </summary>
        /// <param name="plaintext">明文</param>
        /// <param name="publicKey">公钥</param>
        /// <returns>密文字符串</returns>
        public static string EncryptByRSA(string plaintext, string publicKey)
        {
            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            byte[] dataToEncrypt = ByteConverter.GetBytes(plaintext);
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.FromXmlString(publicKey);
                byte[] encryptedData = RSA.Encrypt(dataToEncrypt, false);
                return Convert.ToBase64String(encryptedData);
            }
        }
        /// <summary>
        /// RSA解密
        /// </summary>
        /// <param name="ciphertext">密文</param>
        /// <param name="privateKey">私钥</param>
        /// <returns>明文字符串</returns>
        public static string DecryptByRSA(string ciphertext, string privateKey)
        {
            UnicodeEncoding byteConverter = new UnicodeEncoding();
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.FromXmlString(privateKey);
                byte[] encryptedData = Convert.FromBase64String(ciphertext);
                byte[] decryptedData = RSA.Decrypt(encryptedData, false);
                return byteConverter.GetString(decryptedData);
            }
        }

        //public static string signByRSA(string plaintext, string privateKey)
        //{
        //    UnicodeEncoding ByteConverter = new UnicodeEncoding();
        //    byte[] dataToEncrypt = ByteConverter.GetBytes(plaintext);
        //    using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
        //    {
        //        RSA.FromXmlString(privateKey);
        //        byte[] encryptedData = RSA.SignData(dataToEncrypt,);
        //        return Convert.ToBase64String(encryptedData);
        //    }
        //}
        /// <summary>
        /// 数字签名
        /// </summary>
        /// <param name="plaintext">原文</param>
        /// <param name="privateKey">私钥</param>
        /// <returns>签名</returns>
        public static string HashAndSignString(string plaintext, string privateKey)
        {
            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            byte[] dataToEncrypt = ByteConverter.GetBytes(plaintext);

            using (RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider())
            {
                RSAalg.FromXmlString(privateKey);
                //使用SHA1进行摘要算法，生成签名
                byte[] encryptedData = RSAalg.SignData(dataToEncrypt, new SHA1CryptoServiceProvider());
                return Convert.ToBase64String(encryptedData);
            }
        }
        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="plaintext">原文</param>
        /// <param name="SignedData">签名</param>
        /// <param name="publicKey">公钥</param>
        /// <returns></returns>
        public static bool VerifySigned(string plaintext, string SignedData, string publicKey)
        {
            using (RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider())
            {
                RSAalg.FromXmlString(publicKey);
                UnicodeEncoding ByteConverter = new UnicodeEncoding();
                byte[] dataToVerifyBytes = ByteConverter.GetBytes(plaintext);
                byte[] signedDataBytes = Convert.FromBase64String(SignedData);
                return RSAalg.VerifyData(dataToVerifyBytes, new SHA1CryptoServiceProvider(), signedDataBytes);
            }
        }
        /// <summary>
        /// 获取Key
        /// 键为公钥，值为私钥
        /// </summary>
        /// <returns></returns>
        public static KeyValuePair<string, string> CreateRSAKey()
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            string privateKey = RSA.ToXmlString(true);
            string publicKey = RSA.ToXmlString(false);

            return new KeyValuePair<string, string>(publicKey, privateKey);
        }
        #endregion

        #region other
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] GetBytes(string input)
        {
            string[] sInput = input.Split("-".ToCharArray());
            byte[] inputBytes = new byte[sInput.Length];
            for (int i = 0; i < sInput.Length; i++)
            {
                inputBytes[i] = byte.Parse(sInput[i], NumberStyles.HexNumber);
            }
            return inputBytes;
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
                if (lblLocalAddressIPv4.Text != "")
                {
                    Clipboard.SetDataObject(lblLocalAddressIPv4.Text);
                }
            MetroFramework.MetroMessageBox.Show(this, "Successfully Copy", "Reminder", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}
